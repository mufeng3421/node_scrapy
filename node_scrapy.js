const fs = require('fs');
const path = require('path');
const commander = require('commander');
const _ = require('lodash');
const node_scrapy = require('./core/engine');
const ConsoleTransport = require('egg-logger').ConsoleTransport;
const settings = require(path.join(__dirname, /*FIXME: 将project动态指定*/'project', 'settings'))
const logger = node_scrapy.logger;

logger.set('console', new ConsoleTransport({
    level: settings.LOGGER_LEVEL,
}));


commander.version(node_scrapy.version)

commander.command('crawl [spiderName]')
    .option('-a, --all', 'all spiders')
    .action(function(spiderName, cmd) {
        // console.log('ready to run spider: ' + spiderName)
        spiderName = cmd.all ? null : spiderName;
        const spiders = spiderLoader(spiderName || null);
        const pipelines = pipelineLoader();
        node_scrapy.start(spiders, pipelines);
    })

commander.parse(process.argv);

function spiderLoader(name) { //返回所有
    const spidersDir = path.join(__dirname, /*FIXME: 将project动态指定*/'project', 'spiders')
    const spiderList = fs.readdirSync(spidersDir) || [];
    const spiders = [];
    for (let spiderFileName of spiderList) {
        if (/.js$/.test(spiderFileName)) {
            const spiderName = spiderFileName.split('.')[0];
            let spider;

            if (!name) {
                // 加载全部spider
                spider = require(path.join(spidersDir, spiderName));
            }else {
                // 加载部分
                if (spiderName !== name) continue;
                spider = require(path.join(spidersDir, spiderName));
            }

            if (!spider) continue;
            spiders.push(spider);
            logger.info(`[spiderLoader] spider name is ${ spiderName || ''}, ...loaded.`);
        }
    }
    return spiders;
}

function pipelineLoader() {
    const ITEM_PIPELINES = settings.ITEM_PIPELINES;
    const pipelines = require(path.join(__dirname, /*FIXME: 将project动态指定*/'project', 'pipelines'))
    const pipelineList = [];
    for (let pipeline in pipelines) {
        if (pipeline in ITEM_PIPELINES) {
            pipelineList.push({
                pipeline: pipelines[pipeline],
                priority: ITEM_PIPELINES[pipeline]
            })
            logger.info(`[pipeline loader] pipeline name is ${ pipeline || ''}, ...loaded.`)
        }
    }
    const pipelineListSorted = _.sortBy(pipelineList, function(item) {
        return -item.priority;
    });

    const pipelineResult = [];
    for(let item of pipelineListSorted) {
        pipelineResult.push(item.pipeline)
    }
    return pipelineResult;
}