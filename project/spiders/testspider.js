const Spider = require('../../core/index').Spider;
const testSpiderItem = require('../items').TestSpider;
class TestSpider extends Spider{
    constructor(engine) {
        super(engine);
        this.name = 'test_spider';
        this.encode = 'utf-8';
        this.method = 'GET';
        this.urls = [];
    }

    parse(response) {
        const item = new testSpiderItem();
        item.title = 'this title';
        item.href = 'this href';
        item.time = 'this time';
        return item;
    }
}
module.exports = TestSpider;