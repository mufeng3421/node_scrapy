const Pipeline = require('../core/index').Pipeline;

class spider_pipeline_1 extends Pipeline {
    constructor() {
        super();
    }
    open_spider() {
        return;
    }
    process_item(item, spider) { //子类必须复写
        return;
    }
    spider_close() {
        return;
    }
}

module.exports = {
    spider_pipeline_1,
}