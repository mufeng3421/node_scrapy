const Item = require('../core/index').Item;

class TestSpiderItem extends Item {
    constructor() {
        super();
        const Feild = super.Feild;
        Feild('title');
        Feild('href');
        Feild('time');
    }
}

module.exports = TestSpiderItem;