const _ = require('lodash');
const core = require('./index');
const logger = core.logger;
const EventEmitter = require('events').EventEmitter;
const emitter = new EventEmitter();
const EVENTS = {
    pushOneRequestToScheduler: 'pushOneRequestToScheduler',
    pullOneRequestToDownloader: 'pullOneRequestToDownloader',
    downloadSuccess: 'downloadSuccess',
    downloadFaild: 'downloadFaild',
    pushItemDataToPipelines: 'pushItemDataToPipelines',
    spiderCrawlDone: 'spiderCrawlDone',
}

const Engine = {
    emitter,
    events: EVENTS,
}

const spiderInstanceList = [];  //所有加载过来的spider实例
const pipelineInstanceList = []; //所有加载过来的pipeline 实例

const Scheduler = new core.Scheduler(Engine, 1000); //调度器实例
const Downloader = new core.Downloader(Engine); //下载器实例

const Emitter = Engine.emitter;
// 监听提交Request实例的时间，并将Request实例加入调度器队列 
Emitter.on(EVENTS.pushOneRequestToScheduler, function(requestItem) {
    Scheduler.add(requestItem) // 加入调度器下载队列
});


// 拉取下一个request实例，发给downloader下载
Emitter.on(EVENTS.pullOneRequestToDownloader, function() {
    const requestItem = Scheduler.next();
    Downloader.doRequest(requestItem)
})

// 下载器下载成功
Emitter.on(EVENTS.downloadSuccess, function(response, spider) {
    const resultInstance = spider.parse(response);
    if (!resultInstance) {
        Emitter.emit(EVENTS.pullOneRequestToDownloader);
        return;
    }
    // 处理请求类型
    if (resultInstance instanceof core.Request) {
        // 加入调度器队列
        Scheduler.add(resultInstance);
        Emitter.emit(EVENTS.pullOneRequestToDownloader)  //下载调度器的下一个链接
        return;
    }

    // 处理Item(数据结构类型)
    if (resultInstance instanceof core.Item) {
        const itemInstance = resultInstance;
        Emitter.emit(EVENTS.pushItemDataToPipelines, itemInstance._getValue());
    }
})

// 下载器下载失败
Emitter.on(EVENTS.downloadFaild, function(err, spider) {
    // TODO: 下载失败后的措施
})

// Item 把结构化过的数据传给pipeline
Emitter.on(EVENTS.pushItemDataToPipelines, function (itemObj) {
    for(const piplineObj of pipelineInstanceList) {
        const pipeInstance = piplineObj.pipeline;
        if (!pipeInstance.process_item) {
            logger.warn(`[pipeline] this pipeline instance haven't process_item method.`)
        }else {
            pipeInstance.process_item(itemObj);
        }
    }
});

// 所有爬虫都已经执行结束，退出程序
Emitter.on(EVENTS.spiderCrawlDone, function () {
    // 调用关闭pipeline的spider_close()
    for (let pipeline of pipelineInstanceList) {
        const pipeInstance = pipeline.pipeline;
        if (!!pipeInstance.spider_close) {
            pipeInstance.spider_close();
        }
    }
    logger.info('crawl jobs finished');
    process.exit(); 
});

const EngineExportor = {
    version: '0.0.1',
    Item: core.Item,
    Spider: core.Spider,
    Scheduler: core.Scheduler,
    Downloader: core.Downloader,
    Err: core.Err,
    Response: core.Response,
    Request: core.Request,
    logger,
}

EngineExportor.start = function(spidersClass, piplines) {
    // 在这里启动爬虫，运行爬虫的start_request方法
    if (spidersClass.length > 0) {
        for (const spiderItemClass of spidersClass) {
            spiderInstanceList.push(new spiderItemClass(Engine));
        }
        logger.info('[Engine] all spider instantiation process done.');
    }
    if (piplines.length > 0) {
        for (const pipeline of piplines) {
            pipeline.pipeline = new pipeline.pipeline()
            pipelineInstanceList.push(pipeline);
        }
        logger.info('[Engine] all pipelines instantiation process done.')
    }

    logger.info('[Engine] spider crawl task started.')

    // 检查pipeline的hooks
    for (const pipelineItem of pipelineInstanceList) {
        let p = pipelineItem.pipeline;
        if (!!p.open_spider) {
            p.open_spider();
        }
    }
    for (const spiderInstance of spiderInstanceList) {
        logger.info(`[Engine] spider ['${spiderInstance.name}'] start crawl task`);
        spiderInstance.start_request(); //调用spider的爬取触发函数
    }
}

module.exports = EngineExportor;

