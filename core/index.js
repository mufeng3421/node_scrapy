const Logger = require('egg-logger').Logger;
const logger = new Logger();

// 错误类
class Err {

    static RequiredEngine(engine) {
        if (!engine) {
            Err.Error('engine argument was required')
        }
    }

    static Error(msg) {
        throw new Error(msg || null)
    }

    // 类型错误
    static TypeError(msg) {
        throw new Error(msg || null)
    }

    // 语法错误
    static SyntaxError(msg) {
        throw new SyntaxError(msg || null)
    }

    // 引用错误
    static ReferenceError(msg) {
        throw new ReferenceError(msg || null)
    }
}

// 请求对象
class Request {
    constructor(url, method, encode, spider, callback) {
        this.url = url || '';
        this.method = method || 'GET';
        this.encode = encode || 'utf-8';
        this.callback = callback || null;
        this.headers = '';
        this.body = null;
        this.cookies = ''
        this.priority = 1; //优先级：number,数字越大，优先级越高
        this.spider = spider;
    }
}

// 响应对象
const iconv = require('iconv-lite');
const cheerio = require('cheerio');
class Response {
    constructor(axiosResponse) {
        this.url = axiosResponse.config.url;
        this.method = axiosResponse.config.method;
        this.encode = axiosResponse.config.encode;
        this.data = axiosResponse.data; // 请求响应文本
        this.status = axiosResponse.status; // 状态码
        this.statusText = axiosResponse.statusText; //相应状态文本
        this.responseHeaders = axiosResponse.headers; // 请求响应头
        this.rawResponse = axiosResponse;
    }
    css(cssSelectorStr) {
        const rawBody = iconv.decode(new Buffer(this.data, this.encode), this.encode);
        const $ = cheerio.load(rawBody); //生成cheerio, 类Jquery的selector对象
        return $(cssSelectorStr);
    }
}

// 调度器类
class Scheduler {
    constructor(engine, maxTask) {
        Err.RequiredEngine(engine)
        if (!maxTask || !maxTask && maxTask < 1) {
            maxTask = 1
        }
        this.engine = engine;
        this.maxTask = maxTask; //调度器最大任务数
        this.sequenc = []
    }

    // add to sequenc
    add(RequestItem) {
        // 判断Request对象是否为Request类的实例
        if (!(RequestItem instanceof Request)) {
            Err.TypeError('the argument must be an instance of Request class')
            return;
        }
        this.sequenc.push(Request)
    }

    // pick out next request 
    next() {
        if (this.sequenc.length > 0) {
            return this.sequenc.shift();
        }else {
            // 完成爬取，退出
            this.engine.emitter.emit(this.engine.events.spiderCrawlDone);
        }
    }
}

// 下载器
const axios = require('axios');
class Downloader {
    constructor(engine) {
        Err.RequiredEngine(engine); //判断创立的engine时候正确
        this.engine = engine;
    }
    
    doRequest(requestItem) {
        const $this = this;
        const {
            url,
            method,
            headers,
            body,
            encode,
            spider
        } = requestItem;
        axios({
            url,
            method,
            headers,
            encode,
            data: body,
        }).then((res) => {
            logger.error(`[Downloader] download success ${res.config.method} ${res.status} ${res.statusText} ${res.config.url}`)
            const DecoratedRespose = new Response(res);
            $this.engine.emitter.emit($this.engine.events.downloadSuccess, DecoratedRespose, spider);
        }).catch((err) => {
            logger.error(`[Downloader] download faild: ${err}`)
            $this.engine.emitter.emit($this.engine.events.downloadFaild, err, spider);
        })
    }
}

// Spider基类
class Spider {
    constructor(engine) {
        Err.RequiredEngine(engine);
        this.engine = engine;
        this.name = 'default_spider';
        this.method = '';
        this.encode = '';
        this.urls = [];
    }

    //  _______private 方法________ 
    _requestInstanceFactory(url, callback=this.parse) {
        return new Request(url, this.method, this.encode, this, callback);
    }

    // 开始爬取，生成Request实例
    start_request() {
        for (let url of this.urls) {
            const requestItem = new this._requestInstanceFactory(url);
            this.engine.emitter.emit(this.engine.events.pushOneRequestToScheduler, requestItem)
        }
        this.engine.emitter.emit(this.engine.events.pullOneRequestToDownloader)
    }


    // _______protect 方法________ 
    setMethod_(method) {
        if (!!method || typeof method !== 'string') {
            Err.TypeError('argument type must be string');
            return;
        }
        this.method = method;
    }

    setEncode_(encode) {
        if (!!encode || typeof encode !== 'string') {
            Err.TypeError('argument type must be string');
            return;
        }
        this.method = method;
    }

    setUrls_(urls) {
        if (!Array.isArray(urls)) {
            Err.TypeError('argument type must be array');
            return;
        }
        this.urls = urls;
    }

    // _______public 方法________ 
    parse(response) {
        /* 需要在spider实例中复写 */
        return; 
    }
}

// Item的基类， 所有Item需要继承该基类
class Item {
    constructor() {
        this._itemValue = {};
    }
    Feild(keyName) {
        this._itemValue[keyName] = null;
    }
    _getValue() {
        return this._itemValue;
    }
}

// Pipeline的基类
class Pipeline {
    open_spider() {
        return;
    }
    process_item(item, spider) { //子类必须复写
        return;
    }
    spider_close() {
        return;
    }
}

module.exports = {
    Item,
    Spider,
    Scheduler,
    Downloader,
    Err,
    Response,
    Request,
    logger,
    Pipeline,
}